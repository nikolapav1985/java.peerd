import javax.swing.JOptionPane;
/**
* Quiz class
*
* A quiz example. One reply is correct and makes quiz attempt successful.
* All other replies (single letter) are incorrect (these cause a quiz to continue).
* All other replies having length more than one letter are incorrect and invalid.
*
* compile example ----- javac Quiz.java
*
* run example ------ java Quiz
*
*/
class Quiz{
    public static void main(String[] args){
        int nQuestions = 5; // total number of questions
        int nCorrect = 0; // number of correct attempts
        MultipleChoiceQuestion questiona=new MultipleChoiceQuestion("Best method for data input?",
        "A keyboard","B mouse","C gesture","D voice","E stylus","A");
        MultipleChoiceQuestion questionb=new MultipleChoiceQuestion("Most popular drink in the morning?",
        "A coffee","B tea","C milk","D orange juice","E apple juice","A");
        MultipleChoiceQuestion questionc=new MultipleChoiceQuestion("Best code editor?",
        "A notepad","B notepad++","C emacs","D vim","E visual studio","C");
        MultipleChoiceQuestion questiond=new MultipleChoiceQuestion("Easiest dish to make?",
        "A pot roast","B boiled rice","C steamed rice", "D steamed buns", "E fried buns","C");
        MultipleChoiceQuestion questione=new MultipleChoiceQuestion("Tastiest meat?",
        "A chicken","B beef","C lamb","D goat","E pork","B");
        MultipleChoiceQuestion[] questions=new MultipleChoiceQuestion[nQuestions];
        int i=0;

        questions[0]=questiona;
        questions[1]=questionb;
        questions[2]=questionc;
        questions[3]=questiond;
        questions[4]=questione;

        for(;i<nQuestions;i++){ // ask questions
            questions[i].check();
            nCorrect += questions[i].nCorrect;
        }

        JOptionPane.showMessageDialog(null, "The score is "+nCorrect+" out of "+nQuestions+"!");
    }
}
